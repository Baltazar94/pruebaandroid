package com.example.baltazar.android1.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.baltazar.android1.R;
import com.example.baltazar.android1.Wrappers.PlanetWrapper;
import com.example.baltazar.android1.objetos.Planeta;

/**
 * Created by Baltazar on 04/08/2015.
 */
public class PlanetsAdapter extends ArrayAdapter<Planeta>{

    public PlanetsAdapter(Context context) {
        super(context, 0);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Planeta p = getItem(position);
        PlanetWrapper pw;
        if (convertView==null){
            convertView=View.inflate(getContext(), R.layout.planet_list_item,null);
            pw = new PlanetWrapper(convertView);
            convertView.setTag(pw);
        }
        else{
            pw=(PlanetWrapper)convertView.getTag();
        }

        pw.getTxtPlanetname().setText(p.getNombre());
        pw.getIvPlanet().setImageResource(p.getPlanetaImagen());
        pw.getTxtDistancia().setText(p.getDistancia());
        return convertView;
    }
}
