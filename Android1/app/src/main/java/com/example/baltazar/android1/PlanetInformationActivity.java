package com.example.baltazar.android1;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.baltazar.android1.objetos.Planeta;

import org.w3c.dom.Text;


public class PlanetInformationActivity extends ActionBarActivity {

    private TextView name;
    private ImageView image;
    private TextView distance;
    private TextView description;

    private TextView myToolbarText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_planet_information);

        //TOOLBAR
        myToolbarText=(TextView)findViewById(R.id.toolbarTitle);


        // Fetching data from a parcelable object passed from MainActivity
        Planeta planeta = getIntent().getParcelableExtra("planeta");

        // Getting reference to TextView tv_sname of the layout file activity_student
        //name=(TextView)findViewById(R.id.txtPlanetName);
        image=(ImageView)findViewById(R.id.ivPlanetImage);
        distance=(TextView)findViewById(R.id.txtPlanetDistance);
        description=(TextView)findViewById(R.id.txtPlanetInformation);

        if(planeta!=null){
            myToolbarText.setText(planeta.getNombre());
            //name.setText(planeta.getNombre());
            image.setImageResource(planeta.getPlanetaImagen());
            distance.setText(planeta.getDistancia());
            description.setText(planeta.getDescripcion());
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_planet_information, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
