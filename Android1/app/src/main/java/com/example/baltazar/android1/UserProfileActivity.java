package com.example.baltazar.android1;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.baltazar.android1.objetos.User;
import com.example.baltazar.android1.objetos.UserWork;

import org.w3c.dom.Text;


public class UserProfileActivity extends ActionBarActivity {

    private Button seePlanets;
    private Button logout;
    private UserWork userW;
    private User use;

    private TextView userName;
    private TextView name;
    private TextView lastName;
    private TextView phone;
    private TextView city;

    private TextView myToolbarText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        myToolbarText=(TextView)findViewById(R.id.toolbarTitle);
        myToolbarText.setText("User Profile");

        seePlanets=(Button)findViewById(R.id.btnSeePlanets);
        logout=(Button)findViewById(R.id.btnLogOut);

        userName=(TextView)findViewById(R.id.tvUserName);
        name=(TextView)findViewById(R.id.tvName);
        lastName=(TextView)findViewById(R.id.tvLastName);
        phone=(TextView)findViewById(R.id.tvPhone);
        city=(TextView)findViewById(R.id.tvCity);

        userW = new UserWork(getApplicationContext());

        use = userW.getSavedUser();

        userName.setText(use.get_userName());
        name.setText(use.get_name());
        lastName.setText(use.get_lastName());
        phone.setText(use.get_phone());
        city.setText(use.get_city());

        seePlanets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserProfileActivity.this, NuevaPantalla.class);
                startActivity(intent);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userW.forgetUser();
                Intent intent = new Intent(UserProfileActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
