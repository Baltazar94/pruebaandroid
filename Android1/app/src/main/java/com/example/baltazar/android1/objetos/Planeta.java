package com.example.baltazar.android1.objetos;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Baltazar on 04/08/2015.
 */
public class Planeta implements Parcelable{

    private String nombre;
    private int planetaImagen;
    private String distancia;
    private String descripcion;

    public Planeta(){

    }

    //Constructor
    public Planeta(String nombre,int planetaImagen, String distancia, String descripcion){
        this.nombre=nombre;
        this.planetaImagen=planetaImagen;
        this.distancia=distancia;
        this.descripcion=descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPlanetaImagen() {
        return planetaImagen;
    }

    public void setPlanetaImagen(int planetaImagen) {
        this.planetaImagen = planetaImagen;
    }

    public String getDistancia() {
        return distancia;
    }

    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    //Parcel part
    public void writeToParcel(Parcel dest, int flags){
        dest.writeString(nombre);
        dest.writeInt(planetaImagen);
        dest.writeString(distancia);
        dest.writeString(descripcion);
    }

    private Planeta(Parcel in){
        this.nombre=in.readString();
        this.planetaImagen=in.readInt();
        this.distancia=in.readString();
        this.descripcion=in.readString();
    }

    public static final Parcelable.Creator<Planeta> CREATOR = new Parcelable.Creator<Planeta>() {

        @Override
        public Planeta createFromParcel(Parcel source) {
            return new Planeta(source);
        }

        @Override
        public Planeta[] newArray(int size) {
            return new Planeta[size];
        }
    };

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

}
