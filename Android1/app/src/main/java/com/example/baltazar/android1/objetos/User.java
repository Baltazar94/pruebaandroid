package com.example.baltazar.android1.objetos;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Baltazar on 11/08/2015.
 */
public class User {

    private String _userName;
    private String _name;
    private String _lastName;
    private String _pass;
    private String _phone;
    private String _city;

    //Constructor
    public User() {
    }

    public User(String _userName, String _name, String _lastName, String _pass, String _phone, String _city) {
        this._userName = _userName;
        this._name = _name;
        this._lastName = _lastName;
        this._pass = _pass;
        this._phone = _phone;
        this._city = _city;
    }

    //Getters and Setters
    public String get_userName() {
        return _userName;
    }

    public void set_userName(String _userName) {
        this._userName = _userName;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_lastName() {
        return _lastName;
    }

    public void set_lastName(String _lastName) {
        this._lastName = _lastName;
    }

    public String get_pass() {
        return _pass;
    }

    public void set_pass(String _pass) {
        this._pass = _pass;
    }

    public String get_phone() {
        return _phone;
    }

    public void set_phone(String _phone) {
        this._phone = _phone;
    }

    public String get_city() {
        return _city;
    }

    public void set_city(String _city) {
        this._city = _city;
    }
}
