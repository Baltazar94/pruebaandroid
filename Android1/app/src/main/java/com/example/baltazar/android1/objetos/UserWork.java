package com.example.baltazar.android1.objetos;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Baltazar on 11/08/2015.
 */
public class UserWork {

    private SharedPreferences manager;
    private SharedPreferences.Editor editor;

    private String _user_userName = "user_userName";
    private String _user_name = "user_name";
    private String _user_lastName = "user_lastName";
    private String _user_pass = "user_pass";
    private String _user_phone = "user_phone";
    private String _user_city = "user_city";

    public UserWork(Context context) {
        manager = context.getSharedPreferences("BDUsers",0);
        editor = manager.edit();
    }

    public void saveUser(User user){
        setUserName(user.get_userName());
        setName(user.get_name());
        setLastName(user.get_lastName());
        setPass(user.get_pass());
        setPhone(user.get_phone());
        setCity(user.get_city());
    }

    public User getSavedUser(){
        User user = new User();
        user.set_userName(getUserName());
        user.set_name(getName());
        user.set_lastName(getLastame());
        user.set_pass(getPass());
        user.set_phone(getPhone());
        user.set_city(getCity());
        return user;
    }

    public void forgetUser(){
        forgetUserName();
        forgetName();
        forgetLastName();
        forgetPass();
        forgetPhone();
        forgetCity();
    }

    public boolean existUser(){
        return (getUserName().equals("")) ? false : true;
    }

    //GETTERS AND SETTER DE LAS VARIABLES

    private String getUserName(){
        return manager.getString(_user_userName,"");
    }

    private void setUserName(String userName){
        editor.putString(_user_userName, userName);
        editor.commit();
    }

    private void forgetUserName(){
        editor.remove(_user_userName);
        editor.commit();
    }

    private String getName(){
        return manager.getString(_user_name,"");
    }

    private void setName(String name){
        editor.putString(_user_name, name);
        editor.commit();
    }

    private void forgetName(){
        editor.remove(_user_name);
        editor.commit();
    }

    private String getLastame(){
        return manager.getString(_user_lastName,"");
    }

    private void setLastName(String lastName){
        editor.putString(_user_lastName, lastName);
        editor.commit();
    }

    private void forgetLastName(){
        editor.remove(_user_lastName);
        editor.commit();
    }

    private String getPass(){
        return manager.getString(_user_pass,"");
    }

    private void setPass(String pass){
        editor.putString(_user_pass, pass);
        editor.commit();
    }

    private void forgetPass(){
        editor.remove(_user_pass);
        editor.commit();
    }

    private String getPhone(){
        return manager.getString(_user_phone,"");
    }

    private void setPhone(String phone){
        editor.putString(_user_phone, phone);
        editor.commit();
    }

    private void forgetPhone(){
        editor.remove(_user_phone);
        editor.commit();
    }

    private String getCity(){
        return manager.getString(_user_city,"");
    }

    private void setCity(String city){
        editor.putString(_user_city, city);
        editor.commit();
    }

    private void forgetCity(){
        editor.remove(_user_city);
        editor.commit();
    }

}
