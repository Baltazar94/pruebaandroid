package com.example.baltazar.android1.Wrappers;

import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.baltazar.android1.R;

/**
 * Created by Baltazar on 05/08/2015.
 */
public class PlanetWrapper {

    private TextView txtPlanetname;
    private ImageView ivPlanet;
    private TextView txtDistancia;
    private View base;

    public PlanetWrapper(View base){
        this.base=base;
    }

    public ImageView getIvPlanet() {
        if (ivPlanet==null)
            ivPlanet = (ImageView)base.findViewById(R.id.imageButton);
        return ivPlanet;
    }

    public TextView getTxtPlanetname(){
        if (txtPlanetname==null)
            txtPlanetname=(TextView)base.findViewById(R.id.EditText01);
        return txtPlanetname;
    }

    public TextView getTxtDistancia(){
        if (txtDistancia==null)
            txtDistancia=(TextView)base.findViewById(R.id.tvDistancia);
        return txtDistancia;
    }
}
