package com.example.baltazar.android1;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

//Librerias que Se agregaron
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.baltazar.android1.adapters.PlanetsAdapter;
import com.example.baltazar.android1.objetos.Planeta;

public class NuevaPantalla extends ActionBarActivity implements AdapterView.OnItemClickListener{ //fue necesario implementar para que escuchara los elementos seleccionados

    private ListView vista ;
    private ArrayAdapter<String> listAdapter ;
    private PlanetsAdapter adapter;

    private TextView myToolbarText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva_pantalla);

        myToolbarText=(TextView)findViewById(R.id.toolbarTitle);
        myToolbarText.setText("List of planets");

        // Find the ListView resource.
        vista = (ListView)findViewById( R.id.mainListView );
        adapter=new PlanetsAdapter(getApplicationContext());
        adapter.add(new Planeta("Mercurio",R.drawable.mercurio,getCadenas(R.string.dmercurio),getCadenas(R.string.imercurio)));
        adapter.add(new Planeta("Venus",R.drawable.venus,getCadenas(R.string.dvenus),getCadenas(R.string.ivenus)));
        adapter.add(new Planeta("Tierra",R.drawable.tierra,getCadenas(R.string.dtierra),getCadenas(R.string.itierra)));
        adapter.add(new Planeta("Marte",R.drawable.marte,getCadenas(R.string.dmarte),getCadenas(R.string.imarte)));
        adapter.add(new Planeta("Jupiter",R.drawable.jupiter,getCadenas(R.string.djupiter),getCadenas(R.string.ijupiter)));
        adapter.add(new Planeta("Saturno",R.drawable.saturno,getCadenas(R.string.dsaturno),getCadenas(R.string.isaturno)));
        adapter.add(new Planeta("Urano",R.drawable.urano,getCadenas(R.string.durano),getCadenas(R.string.iurano)));
        adapter.add(new Planeta("Neptuno",R.drawable.neptuno,getCadenas(R.string.neptuno),getCadenas(R.string.ineptuno)));
        adapter.add(new Planeta("Pluton", R.drawable.pluton,getCadenas(R.string.dpluton),getCadenas(R.string.ipluton)));

       /* String[] planets = new String[] { "Mercurio", "Venus", "Tierra", "Marte", "Jupiter", "Saturno", "Urano", "Neptuo", "Pluton"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, planets);*/
        vista.setAdapter(adapter);
        vista.setOnItemClickListener(this); //Usa this porque es a la lista de esta clase a la que esta llamando
    }

    //Al dar click sobre un elemento de la lista se mostrara el nombre del planeta seleccionado
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id){
        Planeta planetaActual = (Planeta)vista.getItemAtPosition(position);
        /*String msg = "Planeta: " +planetaActual.getNombre();
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();*/

        Intent intent = new Intent(this, PlanetInformationActivity.class);
        intent.putExtra("planeta",planetaActual);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nueva_pantalla, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public String getCadenas(int resource){
        return getResources().getString(resource);
    }
}
