package com.example.baltazar.android1;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toolbar;

import com.example.baltazar.android1.objetos.User;
import com.example.baltazar.android1.objetos.UserWork;

import org.w3c.dom.Text;


public class RegisterActivity extends AppCompatActivity {

    private EditText editdUserName;
    private EditText editdName;
    private EditText editdLastName;
    private EditText editdPass;
    private EditText editdPhone;
    private EditText editdCity;
    private Button bSave;
    private Button cancel;

    private User us;
    private UserWork uw;

    private TextView myToolbarText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //TOOLBAR
        myToolbarText=(TextView)findViewById(R.id.toolbarTitle);
        myToolbarText.setText("New User");

        editdUserName=(EditText)findViewById(R.id.etUserName);
        editdName=(EditText)findViewById(R.id.etName);
        editdLastName=(EditText)findViewById(R.id.etLastName);
        editdPass=(EditText)findViewById(R.id.etPass);
        editdPhone=(EditText)findViewById(R.id.etPhone);
        editdCity=(EditText)findViewById(R.id.etCity);

        bSave=(Button)findViewById(R.id.btnSave);
        cancel=(Button)findViewById(R.id.btnCancel);

        bSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                us=new User(editdUserName.getText().toString(),
                            editdName.getText().toString(),
                            editdLastName.getText().toString(),
                            editdPass.getText().toString(),
                            editdPhone.getText().toString(),
                            editdCity.getText().toString());
                Intent intent = new Intent(RegisterActivity.this,UserProfileActivity.class);
                uw = new UserWork(getApplicationContext());
                uw.saveUser(us);
                startActivity(intent);
                finish();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
