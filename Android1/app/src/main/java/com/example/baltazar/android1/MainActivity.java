package com.example.baltazar.android1;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;


import com.example.baltazar.android1.objetos.UserWork;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    //public final static String EXTRA_MESSAGE = "com.example.baltazar.android1.MESSAGE";
   /* private Button boton1;
    private Button boton2;
    private Button boton3;*/

    private Button btnS; //BOTON DE STAR DEL MAIN LAYOUT
    private Button btnR; //Boton de registro
    private EditText editName;
    private EditText editPass;
    private UserWork userW;
    private Toolbar myToolbar;
    private TextView myToolbarText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //TOOLBAR
        myToolbarText=(TextView)findViewById(R.id.toolbarTitle);
        myToolbarText.setText("Welcome User");

        //OTRAS COSAS
        editName=(EditText)findViewById(R.id.etUserName);
        editPass=(EditText)findViewById(R.id.etPass);
        btnS=(Button)findViewById(R.id.btnStart);
        btnR=(Button)findViewById(R.id.btnRegister); btnR.setOnClickListener(this);

        userW=new UserWork(getApplicationContext());

        if (userW.existUser()){
            Intent intent=new Intent(this,UserProfileActivity.class);
            startActivity(intent);
            finish();
        }

        //Recuperamos las preferencias almacenados
        /*SharedPreferences pref=getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        String n=pref.getString("act","");

        if (n.equals("0")){
            Intent intent = new Intent(MainActivity.this, NuevaPantalla.class);
            startActivity(intent);
            finish();
        }
        else {
            btnS.setOnClickListener(this);
        }*/

        /*boton2=(Button)findViewById(R.id.button2);
        boton2.setOnClickListener(this);

        boton3=(Button)findViewById(R.id.button3);
        boton3.setOnClickListener(this);

        boton1=(Button)findViewById(R.id.button);
        boton1.setOnClickListener(this)
        */
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
       switch (v.getId()){
            /*case R.id.button:
                Log.i("Baltazar","Boton presionado");
                break;
            case R.id.button2:
                Toast.makeText(getApplicationContext(),"Boton Presionado", Toast.LENGTH_LONG).show();
                break;
            case R.id.button3:
                Intent intent = new Intent(this, NuevaPantalla.class);
                startActivity(intent);
                break;*/
           case R.id.btnStart:

               SharedPreferences settings=getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
               SharedPreferences.Editor editor = settings.edit();
               editor.putString("act", "0");

               //confirmamos almacenamiento
               editor.commit();

               //Abrimos la nueva pantalla
               Intent intent = new Intent(this, NuevaPantalla.class);
               startActivity(intent);
               finish();

               /*Intent intent = new Intent(this, NuevaPantalla.class);;
               startActivity(intent);*/
               break;
           case R.id.btnRegister:
               Intent intentRegister = new Intent(this,RegisterActivity.class);
               startActivity(intentRegister);
               finish();
               break;
        }
    }
}
